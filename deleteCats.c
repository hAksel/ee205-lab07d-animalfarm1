///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file deleteCats.c
//
///
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h> 

#include "catDatabase.h"
#include "deleteCats.h"
#include "config.h"




void deleteAllCats ( ){
   
   for ( int currentCat = 0; currentCat <= numCats; currentCat++){
      memset ( cats[ currentCat ].name, 0, MAX_NAME_LEN ); 
      cats[ currentCat ].gender = 0; 
      cats[ currentCat ].breed = 0;
      cats[ currentCat ].isFixed = 0; 
      cats[ currentCat ].weight =0; 
      cats[ currentCat ].collarColor1 = 0;
      cats[ currentCat ].collarColor2 = 0;
      cats[ currentCat ].license = 0;
   }
   
   numCats = 0; 
   printf("now the number of cats is [%i]\n", numCats);
}
