//////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - AnimalFarm1 - EE 205 - Spr 2022
///
///
///
/// @file config.h
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#pragma once

#define PROGRAM_NAME "Animal Farm 1"  
