///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
/// Usage: This C file will store cat data.
///   catData base will hold difference aspects of each cat such as
///   the cats weight, gender name and color
/// 
///
///
/// Compilation:
///
/// @file catDatabase.h
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdio.h>
#include <stdbool.h>


#define MAX_NAME_LEN 50 
#define MAX_CATS 1024 

//global variables
extern int     numCats;

//declare enums 
enum           Gender {UNKNOWN_GENDER = 0, FEMALE =1 , MALE = 2 };
enum           Breed  {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
enum           Color  {BLACK, WHITE, RED, BLUE, GREEN, PINK};


//create a struct 
struct Cats {
   char                 name[MAX_NAME_LEN]; 
   enum Gender          gender;
   enum Breed           breed; 
   bool                 isFixed;
   float                weight;
   enum Color           collarColor1;
   enum Color           collarColor2;
   unsigned long long   license;
};

//allow other codes to access instance of Cats structure, cats

extern struct Cats   cats[];


//functions declared in catDatabase.c
//validation functions 
extern bool validateCat       (  const char inputCatName[],
                                 const float inputCatWeight );
extern bool validateName      (  const char inputCatName[] );
extern bool validateWeight    (  const float inputCatWeight );
extern bool validateIndex     (  const int currentCat );
extern bool validateNumCats   ( ); 

//enum to string functions
extern char* genderString ( const enum Gender catGender );
extern char* breedString ( const enum Breed catBreed );
extern char* colorString ( const enum Color collarColor ); 

extern void initializeCats();

