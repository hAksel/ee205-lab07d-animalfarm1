///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file addCats.h
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
/////////////////////////////////////////////////////////////////////////////
#pragma once 

bool addCat (  const char catName[],
               const enum Gender catGender,
               const enum Breed catBreed,
               const bool catIsFixed,
               const float catWeight,
               const enum Color catCollarColor1,
               const enum Color catCollarColor2,
               const unsigned long long catLicense );
