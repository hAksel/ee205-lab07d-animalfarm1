///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - AnimalFarm0 - EE 205 - Spr 2022
///
///
///
/// @file reportCats.h
///
/// @version 1.0
///
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   16  February 2022 
///////////////////////////////////////////////////////////////////////////
#pragma once

extern void printCat ( const int currentCat );

extern void printAllCats ( ); 

extern int findCat (const char catName [] );

